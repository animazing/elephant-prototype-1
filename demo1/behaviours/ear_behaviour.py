from servo_behaviour import ServoBehaviour
from datetime import datetime, timedelta
import logging
from events import Events
from actuators import Actuators


class FlapBehaviour(ServoBehaviour):

    SPEED = 500
    ACCEL = 500
    DECEL = 500

    positions = [
        (2, (180, SPEED, ACCEL, DECEL)),
        (2, (0, SPEED, ACCEL, DECEL)),
        (2, (180, SPEED, ACCEL, DECEL)),
        (2, (0, SPEED, ACCEL, DECEL)),
        (2, (60, SPEED, ACCEL, DECEL)),
    ]


