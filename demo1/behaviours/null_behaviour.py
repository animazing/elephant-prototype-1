from behaviour import Behaviour
from datetime import datetime, timedelta
import logging
from events import Events

class NullBehaviour(Behaviour):

    def __init__(self, name, controls):
        super().__init__(name, controls)

    def update(self):
        if self.active:
            if datetime.now() > self.start_time + timedelta(seconds = 5):
                self.logger.debug("Behaviour completed")
                self.setActive(False)
                Events.send_event(self.name, "BEHAVIOUR_COMPLETE")

