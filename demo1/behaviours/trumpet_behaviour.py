from servo_behaviour import ServoBehaviour
from datetime import datetime, timedelta
import logging
from events import Events
from actuators import Actuators


class TrumpetBehaviour(ServoBehaviour):

    SPEED = 200
    ACCEL = 200
    DECEL = 100

    positions = [
        (1, (90, SPEED, ACCEL, DECEL)),
        (3, (0, SPEED, ACCEL, DECEL)),
        (3, (90, SPEED, ACCEL, DECEL)),
        (1, (45, SPEED, ACCEL, DECEL))
    ]

class FeedBehaviour(ServoBehaviour):

    SPEED = 50
    ACCEL = 50
    DECEL = 50

    positions = [
        (3, (180, SPEED, ACCEL, DECEL)),
        (3, (90, SPEED, ACCEL, DECEL)),
    ]

