from servo_behaviour import ServoBehaviour
from datetime import datetime, timedelta
import logging
from events import Events
from actuators import Actuators


class SitUpBehaviour(ServoBehaviour):

    SPEED = 20
    ACCEL = 100
    DECEL = 100

    positions = [
        (0, (30, SPEED, ACCEL, DECEL)),
    ]
