from servo_behaviour import ServoBehaviour
from datetime import datetime, timedelta
import logging
from events import Events
from actuators import Actuators


class LieDownBehaviour(ServoBehaviour):

    SPEED = 50
    ACCEL = 50
    DECEL = 50

    positions = [
        (0, (180, SPEED, ACCEL, DECEL)),
    ]
