from servo_behaviour import ServoBehaviour
from datetime import datetime, timedelta
import logging
from events import Events
from actuators import Actuators


class NodBehaviour(ServoBehaviour):

    SPEED = 500
    ACCEL = 500
    DECEL = 500

    positions = [
        (1, (20, SPEED, ACCEL, DECEL)),
        (1, (40, SPEED, ACCEL, DECEL)),
        (1, (0, SPEED, ACCEL, DECEL)),
        (1, (40, SPEED, ACCEL, DECEL)),
        (1, (0, SPEED, ACCEL, DECEL)),
        (1, (20, SPEED, ACCEL, DECEL)),
    ]
