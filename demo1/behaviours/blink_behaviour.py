from servo_behaviour import ServoBehaviour
from datetime import datetime, timedelta
import logging
from events import Events
from actuators import Actuators


class BlinkBehaviour(ServoBehaviour):

    SPEED = 300
    ACCEL = 500
    DECEL = 2000


    positions = [
    ]

    def start(self):
        if super().start():
            self.controls.getEyes().eye_wink()
            return True
        else:
            return False

