from servo_behaviour import ServoBehaviour
from datetime import datetime, timedelta
import logging
from events import Events
from actuators import Actuators


class TestLegsUpBehaviour(ServoBehaviour):

    SPEED = 50
    ACCEL = 50
    DECEL = 50

    positions = [
        (0, (0, SPEED, ACCEL, DECEL)),
    ]

class TestLegsDownBehaviour(ServoBehaviour):

    SPEED = 50
    ACCEL = 50
    DECEL = 50

    positions = [
        (0, (180, SPEED, ACCEL, DECEL)),
    ]

class TestHeadUpBehaviour(ServoBehaviour):

    SPEED = 50
    ACCEL = 50
    DECEL = 50

    positions = [
        (1, (90, SPEED, ACCEL, DECEL)),
    ]

class TestHeadDownBehaviour(ServoBehaviour):

    SPEED = 50
    ACCEL = 50
    DECEL = 50

    positions = [
        (1, (00, SPEED, ACCEL, DECEL)),
    ]

class TestTrunkMidBehaviour(ServoBehaviour):

    SPEED = 50
    ACCEL = 50
    DECEL = 50

    positions = [
        (3, (90, SPEED, ACCEL, DECEL)),
    ]

class TestTrunkDownBehaviour(ServoBehaviour):

    SPEED = 50
    ACCEL = 50
    DECEL = 50

    positions = [
        (3, (180, SPEED, ACCEL, DECEL)),
    ]

class TestTrunkUpBehaviour(ServoBehaviour):

    SPEED = 50
    ACCEL = 50
    DECEL = 50

    positions = [
        (3, (00, SPEED, ACCEL, DECEL)),
    ]

class TestEarsBackBehaviour(ServoBehaviour):

    SPEED = 50
    ACCEL = 50
    DECEL = 50

    positions = [
        (2, (0, SPEED, ACCEL, DECEL)),
    ]

class TestEarsForwardBehaviour(ServoBehaviour):

    SPEED = 50
    ACCEL = 50
    DECEL = 50

    positions = [
        (2, (180, SPEED, ACCEL, DECEL)),
    ]

