from servo_behaviour import ServoBehaviour
from datetime import datetime, timedelta
import logging
from events import Events
from actuators import Actuators


class SleepBehaviour(ServoBehaviour):

    SPEED = 20
    ACCEL = 100
    DECEL = 500

    positions = [
        (0, (180, SPEED, ACCEL, DECEL)), 
        (1, (0, SPEED, ACCEL, DECEL))
        # (3, (0, SPEED, ACCEL, DECEL)), 
        # (3, (180, SPEED, ACCEL, DECEL)),
    ]

    def start(self):
        if super().start():
            self.controls.getEyes().eye_close()
            # self.controls.getEyes().eye_open()
            return True
        else:
            return False
