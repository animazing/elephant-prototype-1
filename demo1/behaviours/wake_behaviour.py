from servo_behaviour import ServoBehaviour
from datetime import datetime, timedelta
import logging
from events import Events
from actuators import Actuators


class WakeBehaviour(ServoBehaviour):

    SPEED = 50
    ACCEL = 100
    DECEL = 2000


    positions = [
        (0, (0, SPEED, ACCEL, DECEL)), 
        (1, (45, SPEED, ACCEL, DECEL))
    ]

    def start(self):
        if super().start():
            self.controls.getEyes().eye_open()
            return True
        else:
            return False

