from servo_behaviour import ServoBehaviour
from datetime import datetime, timedelta
import logging
from events import Events
from actuators import Actuators


class ShakeBehaviour(ServoBehaviour):

    SPEED = 500
    ACCEL = 1000
    DECEL = 1000
    
    positions = [
        (0, (30, SPEED, ACCEL, DECEL)),
        (0, (40, SPEED, ACCEL, DECEL)),
        (0, (20, SPEED, ACCEL, DECEL)),
        (0, (50, SPEED, ACCEL, DECEL)),
        (0, (10, SPEED, ACCEL, DECEL)),
        (0, (60, SPEED, ACCEL, DECEL)),
        (0, (0, SPEED, ACCEL, DECEL)),
        (0, (30, SPEED, ACCEL, DECEL)),
    ]

