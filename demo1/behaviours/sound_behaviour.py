from behaviour import Behaviour
from datetime import datetime, timedelta
import logging
from events import Events
import pygame as pg

class SoundBehaviour(Behaviour):

    def __init__(self, name, controls):        
        self.logger = logging.getLogger(__name__)
        freq = 44100    # audio CD quality
        bitsize = -16   # unsigned 16 bit
        channels = 2    # 1 is mono, 2 is stereo
        buffer = 2048   # number of samples (experiment to get right sound)
        pg.mixer.init(freq, bitsize, channels, buffer)
        # optional volume 0 to 1.0
        pg.mixer.music.set_volume(1.0)

        super().__init__(name, controls)

    def start(self):
        if self.getActive():
            # Can only be running once; ignore request to restart for now
            self.logger.debug("Cannot start; already active")
            return False
        else:
            self.setActive(True)
            self.start_time = datetime.now()
            self.logger.debug("Start at %s" % self.start_time.strftime("%H:%M:%S"))
            Events.send_event(self.name, "BEHAVIOUR_STARTED")

            # actually play the sound
            pg.mixer.music.load('audio/Ele_trumpet_loud.mp3')
            pg.mixer.music.play()

            return True

    def stop(self):
        if self.getActive():
            Events.send_event(self.name, "BEHAVIOUR_STOPPED")
            self.setActive(False)
            self.logger.debug("Stopped")
            return True
        else:
            self.logger.debug("Cannot Stop; not active")
            return False

    def update(self):
        if self.active:
            if not pg.mixer.music.get_busy(): 
                Events.send_event(self.name, "BEHAVIOUR_COMPLETE")
                self.setActive(False)

