
class Controls:

    def __init__(self, actuators, eyes):
        self.actuators = actuators
        self.eyes = eyes

    def getActuators(self):
        return self.actuators

    def getEyes(self):
        return self.eyes