from behaviour_manager import BehaviourManager
from behaviours.nod_behaviour import NodBehaviour
from behaviours.null_behaviour import NullBehaviour
from behaviours.sleep_behaviour import SleepBehaviour
from behaviours.wake_behaviour import WakeBehaviour
from behaviours.shake_behaviour import ShakeBehaviour
from behaviours.blink_behaviour import BlinkBehaviour
from behaviours.liedown_behaviour import LieDownBehaviour
from behaviours.situp_behaviour import SitUpBehaviour
from behaviours.trumpet_behaviour import TrumpetBehaviour, FeedBehaviour
from behaviours.ear_behaviour import FlapBehaviour
from behaviours.sound_behaviour import SoundBehaviour
from behaviours.test_behaviour import TestLegsUpBehaviour, TestLegsDownBehaviour, TestHeadUpBehaviour, TestHeadDownBehaviour, TestEarsBackBehaviour, TestEarsForwardBehaviour, TestTrunkDownBehaviour, TestTrunkMidBehaviour, TestTrunkUpBehaviour
from events import Events, EventHandler   
from actuators import Actuators
from ellie import EllieEye

from controls import Controls

# from agt import AlexaGadget
import threading
import json
import argparse
import logging
import sys
import time
import random

#pip3 install paho-mqtt
import paho.mqtt.client as mqtt

# sudo apt install mosquittoquit
# sudo systemctl enable mosquitto.service

# Setup the system
behaviour_config = [
    {"name" : "nod", "priority" : 5, "class" :NodBehaviour},
    {"name" : "shake", "priority" : 5, "class" :ShakeBehaviour},
    {"name" : "test", "priority" : 1, "class" :NullBehaviour},
    {"name" : "sleep", "priority" : 2, "class" :SleepBehaviour},
    {"name" : "wake", "priority" : 2, "class" :WakeBehaviour},
    {"name" : "blink", "priority" : 2, "class" :BlinkBehaviour},
    {"name" : "lie", "priority" : 2, "class" :LieDownBehaviour},
    {"name" : "sit", "priority" : 2, "class" :SitUpBehaviour},
    {"name" : "trumpet", "priority" : 2, "class" :TrumpetBehaviour},
    {"name" : "feed", "priority" : 2, "class" :FeedBehaviour},
    {"name" : "flap", "priority" : 2, "class" :FlapBehaviour},
    {"name" : "noisy", "priority" : 2, "class" :SoundBehaviour},
    {"name" : "legs_up", "priority" : 2, "class" :TestLegsUpBehaviour},
    {"name" : "legs_down", "priority" : 2, "class" :TestLegsDownBehaviour},
    {"name" : "head_up", "priority" : 2, "class" :TestHeadUpBehaviour},
    {"name" : "head_down", "priority" : 2, "class" :TestHeadDownBehaviour},
    {"name" : "trunk_up", "priority" : 2, "class" :TestTrunkUpBehaviour},
    {"name" : "trunk_mid", "priority" : 2, "class" :TestTrunkMidBehaviour},
    {"name" : "trunk_down", "priority" : 2, "class" :TestTrunkDownBehaviour},
    {"name" : "ears_back", "priority" : 2, "class" :TestEarsBackBehaviour},
    {"name" : "ears_front", "priority" : 2, "class" :TestEarsForwardBehaviour},
]

behaviour_config_stub = [
    {"name" : "nod", "priority" : 5, "class" :NullBehaviour},
    {"name" : "test", "priority" : 1, "class" :NullBehaviour},
    {"name" : "sleep", "priority" : 2, "class" :NullBehaviour},
    {"name" : "wake", "priority" : 2, "class" :NullBehaviour},
]

class Handler(mqtt.Client):
    
    def __init__(self, real_actuators, config):
        super(Handler,self).__init__()
        # self.alexa = AnimazingGadget('animazing.ini')
        # self.bm = BehaviourManager(behaviour_config)
        self.ev = Events()
        # self.eh = HarnessHandler()
        # Events.register_handler(self.eh)
        # Events.register_handler(self.alexa)
        self.ev.open()
        self.actuators = real_actuators
        if self.actuators:
            self.act = Actuators()
            self.act.start()
            self.eyes = EllieEye()
            self.eyes.eye_open()
            # self.eyes.eye_closed()
            self.controls = Controls(self.act, self.eyes)
        else:
            print("Failed to start actuators")
            self.controls = Controls(None, None)

        self.bm = BehaviourManager(config, self.controls)

        self.handler_thread = threading.Thread(target=self._main_loop)
        # self.mqttc = mqtt.Client()
        # self.mqttc.on_connect = self.on_connect
        # self.mqttc.on_message = self.on_message 

    def start(self):
        print("Starting handler")
        self.running = True
        # self.bm.startBehaviour('sleep')
        self.handler_thread.start()
        # self.alexa.run_alexa(clear_bt, pair_bt)
        # self.running = False
        self.running = True
        self.connect_async("localhost", 1883, 60)
        self.loop_start()
        print("Handler started")

    def stop_loop(self):
        print("Stopping handler")
        self.running = False
        self.loop_stop()

    def _main_loop(self):
        # Sit in a loop waiting for a new command from the user, and printing the results of running behaviours


        print("Type 'help' for a list of commands")

        while(self.running):
            cmd = input("Enter Command: ").lower().split()
            if len(cmd) > 0:
                if cmd[0] == 'help':
                    print("Available commands: help, quit, list, start, stop")
                elif cmd[0] == 'quit':
                    self.running = False
                elif cmd[0] == 'list':
                    print("Available behaviours:")
                    b = self.bm.getBehaviours()
                    for name in b:
                        print("- %s" % name)
                elif cmd[0] == 'start':
                    if len(cmd) == 2:
                        if self.bm.startBehaviour(cmd[1]):                                   
                            print("Started behaviour: %s " % cmd[1])
                        else:
                            print("Could not start behaviour: %s " % cmd[1])
                    else:
                        print("Please provide behaviour to be started")
                elif cmd[0] == 'stop':
                    print("Stop behaviour - not implemented yet")
                else:
                    print("Unexpected command: %s; try 'help' for a list of valid commands" % cmd[0])

        if self.actuators:
            self.act.stop()
        self.ev.close()
        self.bm.close()
        # self.alexa.disconnect()

    # The callback for when the client receives a CONNACK response from the server.
    def on_connect(self, client, userdata, flags, rc):
        print("Main Connected with result code "+str(rc))

        # Subscribing in on_connect() means that if we lose the connection and
        # reconnect then subscriptions will be renewed.
        client.subscribe("command")

    # The callback for when a PUBLISH message is received from the server.
    def on_message(self, client, userdata, msg):
        print(msg.topic+" "+str(msg.payload))
        cmd_json = json.loads(msg.payload.decode("utf-8"))
        if cmd_json["command"] == 'start':
            if self.bm.startBehaviour(cmd_json["behaviour"]):                                   
                print("Started behaviour: %s " % cmd_json["behaviour"])
            else:
                print("Could not start behaviour: %s " % cmd_json["behaviour"])

class Demo(mqtt.Client):
    def __init__(self):
        super(Demo,self).__init__()
        self.handler_thread = threading.Thread(target=self._demo_loop)
        self.handler_thread.start()
        self.running = True
        # self.mqttc = mqtt.Client()
        # self.mqttc.on_connect = self.on_connect
        # self.mqttc.on_message = self.on_message 

    def stop(self):
        self.running = False

    def _demo_loop(self):
        self.connect_async("localhost", 1883, 60)
        self.loop_start()
        print("Demo thread started")
        time.sleep(10)
        print("Demo sending commands after random delays")
        while self.running:

            self.publish("command", "{\"command\" : \"start\", \"behaviour\" : \"wake\"}")
            time.sleep(10)

            count = 0
            while count < 10:
                behaviour = random.randint(1, 8)
                if behaviour <= 6:
                    self.publish("command", "{\"command\" : \"start\", \"behaviour\" : \"blink\"}")
                elif behaviour <= 8:
                    self.publish("command", "{\"command\" : \"start\", \"behaviour\" : \"trumpet\"}")
                else:
                    pass
                    # self.client.publish("command", "start flap")
                time.sleep(random.randint(2, 5))
                count += 1

            self.publish("command", "{\"command\" : \"start\", \"behaviour\" : \"sleep\"}")
            time.sleep(30)


    # The callback for when the client receives a CONNACK response from the server.
    def on_connect(self, client, userdata, flags, rc):
        print("Demo Connected with result code "+str(rc))

        # Subscribing in on_connect() means that if we lose the connection and
        # reconnect then subscriptions will be renewed.
        # client.subscribe("command")

    # The callback for when a PUBLISH message is received from the server.
    def on_message(self, client, userdata, msg):
        print(msg.topic+" "+str(msg.payload))
        # cmd_json = json.loads(msg.payload.decode("utf-8"))
        # if cmd_json["command"] == 'start':
        #     if self.bm.startBehaviour(cmd_json["behaviour"]):                                   
        #         print("Started behaviour: %s " % cmd_json["behaviour"])
        #     else:
        #         print("Could not start behaviour: %s " % cmd_json["behaviour"])

if __name__ == "__main__":
    print("Animazing Behaviours Test Harness")

    parser = argparse.ArgumentParser()
    parser.add_argument('--stub', action='store_true', required=False,
                        help='Use stub behaviours instead of real actuators')
    parser.add_argument('--debug', required=False, default='INFO',
                        help='Set the debug level (DEBUG or INFO')
    parser.add_argument('--demo', action='store_true', required=False,
                        help='Do simple demo behaviour automatically')
    # parser.add_argument('--pair', action='store_true', required=False,
    #     help='Puts the gadget in pairing/discoverable mode. '
    #     'If you are pairing to a previously paired Echo device, '
    #     'please ensure that you first forget the gadget from the Echo device using the Bluetooth menu in Alexa App or Echo\'s screen.')
    # parser.add_argument('--clear', action='store_true', required=False,
    #     help='Reset gadget by unpairing bonded Echo device and clear config file. '
    #     'Please also forget the gadget from the Echo device using the Bluetooth menu in Alexa App or Echo\'s screen. '
    #     'To put the gadget in pairing mode again, use --pair')
    args = parser.parse_args()

    # log_level = 'logging.'+args.debug
    logging.basicConfig(stream=sys.stdout, level=args.debug.upper())

    if args.stub:
        h = Handler(False, behaviour_config_stub)
    else:
        h = Handler(True, behaviour_config)

    if args.demo:
        print("Starting Demo mode")
        d = Demo()

    h.start()



