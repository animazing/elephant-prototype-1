import queue
import logging
import threading
import time

class EventHandler:

    def __init__(self):
        self.logger = logging.getLogger(__name__)

    def new_event(self, event, name):
        self.logger.debug("Unhandled event: %s from %s" % (event, name))

class Events:

    q = queue.Queue()
    logger = logging.getLogger(__name__)
    handlers = []

    def __init__(self):
        self.handler_thread = threading.Thread(target=self._handle_events)
        self.running = False

    def _handle_events(self):
        while self.running:
            try: 
                event = self.q.get(block=True, timeout=1.0)
                self.logger.info("Handle Event (%s) : %s" % (event[0], event[1]))
                for h in self.handlers:
                    # Ignore filtering for now
                    h.new_event(event[1], event[0])
            except queue.Empty:
                pass 

    def open(self):
        if not self.running:
            self.running = True
            self.handler_thread.start()

    def close(self):
        self.running = False

    @classmethod
    def send_event(cls, source, event):
        cls.logger.info("Send Event (%s) : %s" % (source, event))
        cls.q.put((source,event))

    @classmethod
    def register_handler(cls, object):
        cls.handlers.append(object)

