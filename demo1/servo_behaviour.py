from behaviour import Behaviour
from datetime import datetime, timedelta
import logging
from events import Events
from actuators import Actuators


class ServoBehaviour(Behaviour):

    SPEED = 500
    ACCEL = 3000
    DECEL = 3000

    positions = []

    def __init__(self, name, controls):        
        self.logger = logging.getLogger(__name__)
        self.moving = False
        self.next_position = 0
        super().__init__(name, controls)

    def start(self):
        if self.getActive():
            # Can only be running once; ignore request to restart for now
            self.logger.debug("Cannot start; already active")
            return False
        else:
            self.setActive(True)
            self.start_time = datetime.now()
            self.logger.debug("Start at %s" % self.start_time.strftime("%H:%M:%S"))
            Events.send_event(self.name, "BEHAVIOUR_STARTED")
            self.next_position = 0
            self.moving = False
            return True

    def stop(self):
        if self.getActive():
            Events.send_event(self.name, "BEHAVIOUR_STOPPED")
            self.setActive(False)
            self.logger.debug("Stopped")
            if self.moving:
                self.controls.getActuators().stopAllMovement()
            return True
        else:
            self.logger.debug("Cannot Stop; not active")
            return False

    # Do we poll for movement complete or handle the events? 
    # We probably just want to start new movements based on the update function (?) - is this quick enough??

    def _handle_next_position(self):
        if self.next_position < len(self.positions):
            self.logger.debug("Next Position %d of %d" % (self.next_position, len(self.positions)))
            self.controls.getActuators().setTargetPosition(self.positions[self.next_position][0], self.positions[self.next_position][1])
            self.next_position += 1
            self.moving = True
        else:
            self.logger.debug("Behaviour completed, next pos %d" % self.next_position)
            self.setActive(False)
            Events.send_event(self.name, "BEHAVIOUR_COMPLETE")

    def update(self):
        if self.active:
            if not self.moving: 
                self._handle_next_position()
            elif self.controls.getActuators().isComplete():
                self.moving = False

        # In sequence, set a new position (inc the speed of movement), and then wait for it to be complete

        # A movement is defined as a new position, max speed, acceleration, and deceleration (for now)
        # Actuators.setHeadPosition()

        # if datetime.now() > self.start_time + timedelta(seconds = 5):
        #     self.logger.debug("Behaviour completed")
        #     self.setActive(False)
        #     Events.send_event(self.name, "BEHAVIOUR_COMPLETE")

    def new_event(self, event, name):
        if (self.active):
            if event == "MOVEMENT_COMPLETE" and name == "Actuator": 
                self.moving = False
                self._handle_next_position()
