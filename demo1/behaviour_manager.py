import logging
import sys
import threading
import time
from events import EventHandler   

class BehaviourManager(EventHandler):

    def __init__(self, config, controls):
        self.logger = logging.getLogger(__name__)
        self.config = config
        self.behaviours = {}
        for b in config:
            self.behaviours[b["name"]] = b["class"](b["name"], controls)
        self.running = True
        self.update_thread = threading.Thread(target=self._update)
        self.update_thread.start()

    def _update(self):
        while self.running:
            for b in self.behaviours.values():
                if b.getActive():
                    b.update()
            time.sleep(0.1)            

    def startBehaviour(self, behaviour):
        self.logger.info("Start Behaviour: %s" % behaviour)
        if behaviour in self.behaviours.keys():
            return self.behaviours[behaviour].start()
        else:
            return False

    def stopBehaviour(self, behaviour):
        self.logger.info("Stop Behaviour %s" % behaviour)
        if behaviour in self.behaviours.keys():
            return self.behaviours[behaviour].stop()
        else:
            return False

    def getBehaviours(self):
        # print("Return a list of the behaviours")
        names = []
        for b in self.config:
            names.append(b["name"])
        return names

    def new_event(self, event, name):
        if self.running:
            for b in self.behaviours.values():
                if b.getActive():
                    b.new_event(event, name)
 
    def close(self):
        self.running = False

