import pi_servo_hat
import time

MIN_RAMNGE = -30
MAX_RANGE = 150
RANGE_STEP = 1

CHANNEL = 0

DELAY = 0.01

driver = pi_servo_hat.PiServoHat()

driver.restart()

while True:
    for angle in range(MIN_RAMNGE, MAX_RANGE, RANGE_STEP):
        driver.move_servo_position(CHANNEL, angle)
        print("Angle: " + str(angle))
        time.sleep(DELAY)

    for angle in range(MAX_RANGE, MIN_RAMNGE, -RANGE_STEP):
        driver.move_servo_position(CHANNEL, angle)
        print("Angle: " + str(angle))
        time.sleep(DELAY)


