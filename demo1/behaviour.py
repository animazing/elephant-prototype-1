# Super class for the behaviours, defining the standard interface that each behavior will provide.

from events import EventHandler, Events   
import logging
from datetime import datetime, timedelta

class Behaviour(EventHandler):

    def __init__(self, name, controls):
        self.active = False
        self.name = name
        self.controls = controls
        self.logger = logging.getLogger(__name__)

    def setActive(self, active):
        self.active = active

    def getActive(self):
        return self.active

    def start(self):
        if self.getActive():
            # Can only be running once; ignore request to restart for now
            self.logger.debug("Cannot start; already active")
            return False
        else:
            self.setActive(True)
            self.start_time = datetime.now()
            self.logger.debug("Start at %s" % self.start_time.strftime("%H:%M:%S"))
            Events.send_event(self.name, "BEHAVIOUR_STARTED")
            return True

    def stop(self):
        if self.getActive():
            Events.send_event(self.name, "BEHAVIOUR_STOPPED")
            self.setActive(False)
            self.logger.debug("Stopped")
            return True
        else:
            self.logger.debug("Cannot Stop; not active")
            return False

    def new_event(self, event, name):
        self.logger.debug("Unhandled Behaviour event: %s from %s" % (event, name))
    
    def update(self):
        pass


    
