from servo_controller import ServoController
import threading
import time
import logging
from events import Events

# Head positions are angles (in degrees) from a home position. 
# Up / clockwise / right is positive.
# All values are in degrees / degrees per second / degrees per second per second
# A movement is made up of a new target angle, a max speed, and start and end acceleration rates
# (May need to add a smoothness factor in later, or make movements up from smaller steps)
# Only axes that can be controlled (for now) are head pan and head tilt

class Actuators:

    NUMBER_SERVOS = 4

    HOME_POSITION = [180, 0, 180, 90]  # legs start upright for now
    # HOME_POSITION = [180, 0, 180, 90]  
    # # servos = []
    # moving = False
    # target_set= False
    # target_position = []
    # # logger = None
    # lock = threading.Lock()
    # logger = logging.getLogger(__name__)

    def __init__(self):
        self.handler_thread = threading.Thread(target=self._update_actuators)
        self.running = False
        self.moving = False
        self.target_set = False
        # self.target_position = [(0, 0, 0, 0)] * 4
        self.target_position = [(self.HOME_POSITION[0], 0, 0, 0), (self.HOME_POSITION[1], 0, 0, 0), (self.HOME_POSITION[2], 0, 0, 0), (self.HOME_POSITION[3], 0, 0, 0)] 
        self.lock = threading.Lock()
        self.logger = logging.getLogger(__name__)
 
    def setTargetPosition(self, axis, position):
        self.logger.info("Set position: %s" % str(position))
        self.lock.acquire()
        self.target_set = True
        self.target_position[axis] = position
        self.lock.release()

    def stopAllMovement(self):
        self.logger.info("stopAllMovement - not implemented yet")

    # @classmethod
    # def setHeadPosition(cls, position):
    #     # cls.logger.info("Set head position: %s" % str(position))
    #     cls.lock.acquire()
    #     cls.logger.debug("SetHeadPosition %s" % str(position))
    #     cls.target_set = True
    #     cls.target_position = position
    #     cls.lock.release()
        
    # @classmethod
    # def stopHeadMovement(cls):
    #     #

    def start(self):
        if not self.running:
            self.running = True
            ServoController.start_driver()
            if ServoController.isFailed():
                self.logger.error("Servo Controller failed; check hardware")
            else:
                self.logger.info("Started")
                self.handler_thread.start()
                self.servos = [
                    ServoController(2, self.HOME_POSITION[0], -30, -30, 150), 
                    ServoController(3, self.HOME_POSITION[1], -30, -10, 80),  # Range reduced to ensure that we don't hit the end of the slot 
                    ServoController(0, self.HOME_POSITION[2], -30, -30, 150), 
                    ServoController(1, self.HOME_POSITION[3], -30, -30, 150)]
                    # ServoController(2, 180, -30, -30, 150), 
                    # ServoController(3, self.HOME_POSITION[1], -30, -10, 120),  # Range reduced to ensure that we don't hit the end of the slot 
                    # ServoController(0, self.HOME_POSITION[2], -30, -30, 150), 
                    # ServoController(1, 90, -30, -30, 150)]
                # self.servos = [
                #     ServoController(0, self.HOME_POSITION[0], 75, 0, 150), 
                #     ServoController(1, self.HOME_POSITION[1], 30, 0, 180), 
                #     ServoController(2, self.HOME_POSITION[2], 0, -90, 150), 
                #     ServoController(3, self.HOME_POSITION[3], -45, -45, 90)]

    def stop(self):
        self.logger.info("Stopped")
        self.running = False

    # @classmethod
    # def isComplete(cls):
    #     # cls.logger.debug("Complete %s / %s" % (str(cls.moving), str(cls.target_set)))
    #     return not (cls.moving or cls.target_set)

    def isComplete(self):
        return not (self.moving or self.target_set)

    def _update_actuators(self):
        # count = 0
        current_time = time.monotonic()
        while self.running:
            self.lock.acquire()
            # self.logger.debug("Pre %s / %s" % (str(self.moving), str(self.target_set)))
            if self.target_set:
                self.logger.info("Set servo position: %s" % str(self.target_position))
                for servo in range(len(self.target_position)):
                    self.servos[servo].setPosition(self.target_position[servo])
                self.moving = True
                self.target_set= False
            self.lock.release()
            # self.logger.debug("Post %s / %s" % (str(self.moving), str(self.target_set)))

            finished = True
            # update each position for now
            new_time = time.monotonic()
            time_diff = new_time - current_time
            current_time = new_time
            if self.moving:
                for s in self.servos:
                    s.update(time_diff)
                    if s.isMoving():
                        finished = False
                if finished:
                    # Send an event
                    Events.send_event("Actuator", "MOVEMENT_COMPLETE")
                    self.moving = False

            # if count % 100 == 0:
            #     self.logger.debug("running")       
            # count += 1 
            time.sleep(0.05) 

