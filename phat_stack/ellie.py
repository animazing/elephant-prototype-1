#!/usr/bin/env python3
import sys

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
from PIL import ImageOps

import ST7789

import time

display_type = "square"

WIDTH = 240 #disp_l.width
HEIGHT = 240 #disp_l.height
X_MID = WIDTH / 2
Y_MID = (WIDTH / 2) - 2

EYE_RADIUS = 50
EYE_BORDER = EYE_RADIUS + 10

PUPIL_FACTOR = 1.2

BACKGROUND_COLOUR = (0x66, 0x2b, 0x33)
EYE_COLOUR = (0xff, 0xaa, 0x66)
PUPIL_COLOUR = (0, 0, 0)
EYE_BORDER_COLOUR = (0, 0, 0)

BACKLIGHT_PIN=20
DATA_COMMAND_PIN=9

class EllieEye:

    def __init__(self):

        # Create ST7789 LCD display class.
        self.disp_l = ST7789.ST7789(
            port=0,
            cs=ST7789.BG_SPI_CS_FRONT,  # BG_SPI_CS_BACK or BG_SPI_CS_FRONT
            # cs=ST7789.BG_SPI_CS_BACK,  # BG_SPI_CS_BACK or BG_SPI_CS_FRONT
            dc=DATA_COMMAND_PIN,
            backlight=BACKLIGHT_PIN,               # 18 for back BG slot, 19 for front BG slot.
            rotation=0,
            spi_speed_hz=80 * 1000 * 1000,
            offset_left=40 if display_type == "round" else 0
        )

        self.disp_r = ST7789.ST7789(
            port=0,
            # cs=ST7789.BG_SPI_CS_FRONT,  # BG_SPI_CS_BACK or BG_SPI_CS_FRONT
            cs=ST7789.BG_SPI_CS_BACK,  # BG_SPI_CS_BACK or BG_SPI_CS_FRONT
            dc=DATA_COMMAND_PIN,
            backlight=BACKLIGHT_PIN,               # 18 for back BG slot, 19 for front BG slot.
            rotation=0,
            spi_speed_hz=80 * 1000 * 1000,
            offset_left=40 if display_type == "round" else 0
        )

        # Initialize display.
        self.disp_l.begin()
        self.disp_r.begin()

        self.draw_eye(15, 0, 100)


    # pupil_size is in range 1..100
    # pupil direction is in range -100 to 100 (left to right); 
    # eyelid_open is in range 0..100 (100 = fully open)
    def draw_eye(self, pupil_size, pupil_direction, eyelid_open):
        # Define image with a black background 
        img = Image.new('RGBA', (WIDTH, HEIGHT), color=BACKGROUND_COLOUR)
        # img = Image.open("ele_eye_trans.gif")
        draw = ImageDraw.Draw(img)

        # Draw eye outline
        draw.ellipse((X_MID + pupil_direction - EYE_BORDER, Y_MID + (pupil_direction/3) - EYE_BORDER, X_MID + pupil_direction + EYE_BORDER, Y_MID + (pupil_direction/3) + EYE_BORDER), outline=EYE_BORDER_COLOUR, fill=EYE_BORDER_COLOUR)
        draw.ellipse((X_MID + pupil_direction - EYE_RADIUS, Y_MID + (pupil_direction/3) - EYE_RADIUS, X_MID + pupil_direction + EYE_RADIUS, Y_MID + (pupil_direction/3) + EYE_RADIUS), outline=EYE_COLOUR, fill=EYE_COLOUR)


        # iris = Image.open("iris.gif").convert("RGBA")
        # img.paste(iris, (int(X_MID + pupil_direction - EYE_RADIUS), int(Y_MID + (pupil_direction/3) - EYE_RADIUS), int(X_MID + pupil_direction + EYE_RADIUS), int(Y_MID + (pupil_direction/3) + EYE_RADIUS)))

        # draw pupil
        draw.ellipse((X_MID + (pupil_direction*PUPIL_FACTOR)- pupil_size, Y_MID + (pupil_direction/3)- pupil_size, X_MID + (pupil_direction*PUPIL_FACTOR) + pupil_size, Y_MID + (pupil_direction/3) + pupil_size), outline=PUPIL_COLOUR, fill=PUPIL_COLOUR)

        # draw eyelid
        # Need to fill outside of an elipse or similar; maybe draw the top and bottom separately?
        #(or have the outline (and the main part of the eye) as an image and then fill in the eyelid closing?)

        if eyelid_open > 80:
            eyelid = Image.open("ele_eye_trans.gif").convert("RGBA")
        elif eyelid_open > 60:
            eyelid = Image.open("ele_eye_trans_2.gif").convert("RGBA")
        elif eyelid_open > 40:
            eyelid = Image.open("ele_eye_trans_3.gif").convert("RGBA")
        elif eyelid_open > 20:
            eyelid = Image.open("ele_eye_trans_4.gif").convert("RGBA")
        else:
            eyelid = Image.open("ele_eye_trans_5.gif").convert("RGBA")

        img.paste(eyelid, mask = eyelid)

        img_l = ImageOps.mirror(img)
        self.disp_l.display(img_l)

        img_r = ImageOps.flip(img_l)
        self.disp_r.display(img_r)


    def eye_scan(self):
        for pos in range(0, -50, -6):
            self.draw_eye(15, pos, 100)
            # time.sleep(0.01)
        for pos in range(-50, 50, 6):
            self.draw_eye(15, pos, 100)
            # time.sleep(0.01)
        for pos in range(50, 0, -6):
            self.draw_eye(15, pos, 100)
            # time.sleep(0.01)

    def eye_forward(self):
        for pos in range(0, 50, 6):
            self.draw_eye(15, pos, 100)

    def eye_center(self):
        for pos in range(50, 0, -6):
            self.draw_eye(15, pos, 100)
        self.draw_eye(15, 0, 100)

    def eye_dilate(self):
        for size in range(15, 35, 2):
            self.draw_eye(size, 0, 100)
            # time.sleep(0.01)
        for size in range(35, 15, -2):
            self.draw_eye(size, 0, 100)
            # time.sleep(0.01)

    def eye_wink(self):
        self.eye_close()
        self.eye_open()

    def eye_close(self):
        for lid in range(100, 0, -20):
            self.draw_eye(15, 0, lid)
            # time.sleep(0.01)

    def eye_open(self):
        for lid in range(0, 100, 20):
            self.draw_eye(15, 0, lid)
            # time.sleep(0.01)
        self.draw_eye(15, 0, 100)

# draw_eye(15, 0, 100)
# time.sleep(1)
# eye_scan()
# draw_eye(15, 0, 100)
# time.sleep(1)
# eye_dilate()
# draw_eye(15, 0, 100)
# time.sleep(1)
# eye_wink()
# draw_eye(15, 0, 100)
