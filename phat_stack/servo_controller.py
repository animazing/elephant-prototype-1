# Control a servo motor to a specified location with a speed profile.

import pi_servo_hat
import logging

class ServoController:

    DIST_EPSILLON = 1
    UPDATE_RATE = 0.02
    driver = None
    failed = False

    def __init__(self, channel, position, offset, min_a, max_a):
        self.logger = logging.getLogger(__name__)
        self.channel = channel
        self.target_position = position
        self.current_position = position
        self.offset = offset
        self.min_angle = min_a
        self.max_angle = max_a
        self.speed = 0
        self.moving = False
        # Move to target position
        self._sendServoCommand(self.current_position)

    @classmethod
    def start_driver(cls):
        # global driver
        try: 
            print ("Starting servo hat")
            cls.driver = pi_servo_hat.PiServoHat()
            print ("Started servo hat")
            cls.driver.restart()
        except:
            cls.driver = None
            print("servo hat failed")

    @classmethod
    def isFailed(cls):
        return cls.driver == None

    def setPosition(self, position):
        self.target_position = position
        if self.target_position[0] == self.current_position:
            self.logger.info("Keep Position for chan %d : %d" % (self.channel, position[0]))
            self.moving = False
        else:
            # self.logger.info("New Position for chan %d : %d" % (self.channel, position[0]))
            self.moving = True
            self.direction = (self.target_position[0] - self.current_position) / abs(self.target_position[0] - self.current_position)
            self.logger.info("New Position for chan %d : %d (dir %d)" % (self.channel, position[0], self.direction))

    def update(self):
        if self.moving:
            arrived = False
            # calculate if we need to start slowing down
            dist_remaining = self.target_position[0] - self.current_position
            # self.logger.info("Update (chan %d) t: %f,c: %f,d: %f" % (self.channel, self.target_position[0], self.current_position, dist_remaining))
            if dist_remaining * self.direction > 0:
                braking_dist = (self.speed**2) / (2 * self.target_position[3])
                # self.logger.info("Update - not arrived (chan %d) b: %f,d: %f, s: %f" % (self.channel, braking_dist, dist_remaining, self.speed))
                if abs(dist_remaining) < braking_dist:
                    self.speed -= (self.target_position[3] * self.UPDATE_RATE)
                    if self.speed < 0:
                        self.speed = 0
                        arrived = True
                elif self.speed < self.target_position[1]:
                    self.speed += (self.target_position[2] * self.UPDATE_RATE)
            else:
                self.logger.info("Update - arrived (chan %d)" % (self.channel))
                arrived = True

            if arrived:
                # Set current position to be target position and indicate movement complete
                self.current_position = self.target_position[0]
                self._sendServoCommand(self.current_position)
                self.speed = 0
                self.moving = False
            else:
                self.current_position += (self.speed * self.direction) * self.UPDATE_RATE
                self._sendServoCommand(self.current_position)

    def isMoving(self):
        return self.moving

    def _sendServoCommand(self, angle):
        real_angle = angle + self.offset
        if real_angle < self.min_angle:
            real_angle = self.min_angle
        if real_angle > self.max_angle:
            real_angle = self.max_angle
        if not self.driver == None:
            self.driver.move_servo_position(self.channel, real_angle)
        # self.logger.debug("Servo Command %d => %d" % (angle, real_angle))


