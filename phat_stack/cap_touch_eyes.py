import time
import board
import busio
import adafruit_mpr121
import threading

from servo_controller import ServoController
from ellie import EllieEye

running = True
servos = [ServoController(0, 90, 0, 0, 180),
          ServoController(1, 90, 0, 0, 180),
          ServoController(2, 90, 0, 0, 180),
          ServoController(3, 90, 0, 0, 180)]

eyes = EllieEye()

def servo_update():
    while running:
        for s in servos:
            s.update()
        time.sleep(0.05) 

i2c = busio.I2C(board.SCL, board.SDA)
mpr121 = adafruit_mpr121.MPR121(i2c)

prev_state = []
for i in range(12):
    prev_state.append(False)

servo_thread = threading.Thread(target=servo_update)
servo_thread.start()

ServoController.start_driver()

for s in servos:
    s.setPosition((90, 500, 2000, 2000))

while True:
    for i in range(12):
        if mpr121[i].value and not prev_state[i]:
            print('Input {} touched!'.format(i))
            prev_state[i] = True
            if i==0:
                servos[0].setPosition((45, 500, 2000, 2000))
            elif i==1:
                servos[0].setPosition((135, 500, 2000, 2000))
            elif i==2:
                servos[1].setPosition((45, 500, 2000, 2000))
            elif i==3:
                servos[1].setPosition((135, 500, 2000, 2000))
            elif i==4:
                servos[2].setPosition((45, 500, 2000, 2000))
            elif i==5:
                servos[2].setPosition((135, 500, 2000, 2000))
            elif i==6:
                servos[3].setPosition((45, 500, 2000, 2000))
            elif i==7:
                servos[3].setPosition((135, 500, 2000, 2000))
            elif i==8:
                eyes.eye_close()
            elif i==9:
                eyes.eye_forward()
        elif not mpr121[i].value and prev_state[i]:
            print('Input {} released!'.format(i))
            prev_state[i] = False
            if i==8:
                eyes.eye_open()
            elif i==9:
                eyes.eye_center()
