import argparse
import tkinter as tk
import time
import paho.mqtt.client as mqtt
from playsound import playsound

# broker_addr = "192.168.195.231"
# broker_addr = "192.168.1.115"
default_broker_addr = "192.168.4.1"

mqtt_client = None
lbl = None
window = None

def clicked():
    lbl.configure(text="Button was clicked !!")

def quit_clicked():
    window.destroy()

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    lbl.configure(text="Connected with code " + str(rc))

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))

def wake_clicked():
    mqtt_client.publish("command", "{\"command\" : \"start\", \"behaviour\" : \"wake\"}")

def sleep_clicked():
    mqtt_client.publish("command", "{\"command\" : \"start\", \"behaviour\" : \"sleep\"}")

def lie_clicked():
    mqtt_client.publish("command", "{\"command\" : \"start\", \"behaviour\" : \"lie\"}")

def sit_clicked():
    mqtt_client.publish("command", "{\"command\" : \"start\", \"behaviour\" : \"sit\"}")

def blink_clicked():
    mqtt_client.publish("command", "{\"command\" : \"start\", \"behaviour\" : \"blink\"}")

def trumpet_clicked():
    mqtt_client.publish("command", "{\"command\" : \"start\", \"behaviour\" : \"trumpet\"}")

def feed_clicked():
    mqtt_client.publish("command", "{\"command\" : \"start\", \"behaviour\" : \"feed\"}")

def nod_clicked():
    mqtt_client.publish("command", "{\"command\" : \"start\", \"behaviour\" : \"nod\"}")

def flap_clicked():
    mqtt_client.publish("command", "{\"command\" : \"start\", \"behaviour\" : \"flap\"}")

def legs_up_clicked():
    mqtt_client.publish("command", "{\"command\" : \"start\", \"behaviour\" : \"legs_up\"}")

def legs_down_clicked():
    mqtt_client.publish("command", "{\"command\" : \"start\", \"behaviour\" : \"legs_down\"}")

def neck_up_clicked():
    mqtt_client.publish("command", "{\"command\" : \"start\", \"behaviour\" : \"head_up\"}")

def neck_down_clicked():
    mqtt_client.publish("command", "{\"command\" : \"start\", \"behaviour\" : \"head_down\"}")

def trunk_up_clicked():
    mqtt_client.publish("command", "{\"command\" : \"start\", \"behaviour\" : \"trunk_up\"}")

def trunk_mid_clicked():
    mqtt_client.publish("command", "{\"command\" : \"start\", \"behaviour\" : \"trunk_mid\"}")

def trunk_down_clicked():
    mqtt_client.publish("command", "{\"command\" : \"start\", \"behaviour\" : \"trunk_down\"}")

def ears_front_clicked():
    mqtt_client.publish("command", "{\"command\" : \"start\", \"behaviour\" : \"ears_front\"}")

def ears_back_clicked():
    mqtt_client.publish("command", "{\"command\" : \"start\", \"behaviour\" : \"ears_back\"}")

def sound1_clicked():
    playsound('sound/elephant.wav')


def run_ui(broker_addr, test_enabled):
    global mqtt_client, lbl, window
    mqtt_client = mqtt.Client()
    mqtt_client.on_connect = on_connect
    mqtt_client.on_message = on_message
    mqtt_client.connect_async(broker_addr, 1883, 60)
    mqtt_client.loop_start()

    window = tk.Tk()

    window.title("Anibotics Test Control App")

    window.geometry('500x500')

    lbl = tk.Label(window, text="Connecting to " + broker_addr)
    lbl.grid(column=0, row=0)

    # btn = tk.Button(window, text="Click Me", command=clicked)
    quit_btn = tk.Button(window, text="Quit", command=quit_clicked, padx=5, pady=5)

    wake_btn = tk.Button(window, text="Wake", command=wake_clicked, padx=5, pady=5)
    sleep_btn = tk.Button(window, text="Sleep", command=sleep_clicked, padx=5, pady=5)
    lie_btn = tk.Button(window, text="Lie Down", command=lie_clicked, padx=5, pady=5)
    sit_btn = tk.Button(window, text="Sit up", command=sit_clicked, padx=5, pady=5)
    blink_btn = tk.Button(window, text="Blink", command=blink_clicked, padx=5, pady=5)
    trumpet_btn = tk.Button(window, text="Trumpet", command=trumpet_clicked, padx=5, pady=5)
    feed_btn = tk.Button(window, text="Feed", command=feed_clicked, padx=5, pady=5)
    nod_btn = tk.Button(window, text="Nod", command=nod_clicked, padx=5, pady=5)
    flap_btn = tk.Button(window, text="Flap", command=flap_clicked, padx=5, pady=5)

    sound1_btn = tk.Button(window, text="Sound 1", command=sound1_clicked, padx=5, pady=5)

    if test_enabled:
        test_legs_up_btn = tk.Button(window, text="Test Legs Up", command=legs_up_clicked, padx=5, pady=5)
        test_legs_down_btn = tk.Button(window, text="Test Legs Down", command=legs_down_clicked, padx=5, pady=5)
        test_neck_up_btn = tk.Button(window, text="Test Head Up", command=neck_up_clicked, padx=5, pady=5)
        test_neck_down_btn = tk.Button(window, text="Test Head Down", command=neck_down_clicked, padx=5, pady=5)
        test_trunk_up_btn = tk.Button(window, text="Test Trunk Up", command=trunk_up_clicked, padx=5, pady=5)
        test_trunk_mid_btn = tk.Button(window, text="Test Trunk Middle", command=trunk_mid_clicked, padx=5, pady=5)
        test_trunk_down_btn = tk.Button(window, text="Test Trunk Down", command=trunk_down_clicked, padx=5, pady=5)
        test_ears_front_btn = tk.Button(window, text="Test Ears Front", command=ears_front_clicked, padx=5, pady=5)
        test_ears_back_btn = tk.Button(window, text="Test Ears Back", command=ears_back_clicked, padx=5, pady=5)

    wake_btn.grid(column=1, row=1)
    sleep_btn.grid(column=2, row=1)
    lie_btn.grid(column=1, row=2)
    sit_btn.grid(column=2, row=2)

    blink_btn.grid(column=1, row=3)
    trumpet_btn.grid(column=2, row=3)
    feed_btn.grid(column=1, row=4)
    nod_btn.grid(column=2, row=4)
    flap_btn.grid(column=1, row=5)

    quit_btn.grid(column=1, row=8)

    sound1_btn.grid(column=4, row=1)

    if test_enabled:
        test_legs_up_btn.grid(column=6, row=1)
        test_legs_down_btn.grid(column=6, row=2)
        test_neck_up_btn.grid(column=6, row=3)
        test_neck_down_btn.grid(column=6, row=4)
        test_trunk_up_btn.grid(column=6, row=5)
        test_trunk_mid_btn.grid(column=6, row=6)
        test_trunk_down_btn.grid(column=6, row=7)
        test_ears_front_btn.grid(column=6, row=8)
        test_ears_back_btn.grid(column=6, row=9)

    window.mainloop()

if __name__ == "__main__":
    print("Animazing Behaviours UI")

    parser = argparse.ArgumentParser()
    parser.add_argument('--test', action='store_true', required=False,
                        help='Enable the test behaviours')
    parser.add_argument('--broker', default=default_broker_addr,
                        help='Set the IP address of the broker')
    # parser.add_argument('--pair', action='store_true', required=False,
    #     help='Puts the gadget in pairing/discoverable mode. '
    #     'If you are pairing to a previously paired Echo device, '
    #     'please ensure that you first forget the gadget from the Echo device using the Bluetooth menu in Alexa App or Echo\'s screen.')
    # parser.add_argument('--clear', action='store_true', required=False,
    #     help='Reset gadget by unpairing bonded Echo device and clear config file. '
    #     'Please also forget the gadget from the Echo device using the Bluetooth menu in Alexa App or Echo\'s screen. '
    #     'To put the gadget in pairing mode again, use --pair')
    args = parser.parse_args()

    run_ui(args.broker, args.test)
